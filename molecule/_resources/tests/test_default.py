import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_service(host):
    srv = host.service("clamd@scan")
    assert srv.is_running
    assert srv.is_enabled


def test_socket(host):
    host.socket("unix:///var/run/clamd.scan").is_listening


def test_clamd_ping(host):
    host.run_expect([0], "clamdscan --ping 1")
