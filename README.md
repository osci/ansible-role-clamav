# Ansible role for ClamAV installation

[ClamAV](https://www.clamav.net/) is an open source antivirus engine for detecting trojans, viruses, malware & other malicious threats.

This roles installs and configure Clamd, the daemonized version, to run on systems who need regular checks (mail servers for example). Freshclam, used to update the virus database regularly is also installed.

The configuration also enables access via the UNIX socket; by default any user can request a check but this can be changed with `restrict_unix_socket`. Additionally you can enable the network socket too with `with_tcp`; the firewall configuration is left to your care.

The daemon is configured to run as root to be able to check any file not protected by SELinux on the filesystem.

Currently this role only handles Red Hat systems (but contributions are welcome).

Example:

```
- hosts: mx1.example.com
  tasks:
    - include_role:
        name: clamav
      vars:
        max_threads: 50
```

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role clamav

```
